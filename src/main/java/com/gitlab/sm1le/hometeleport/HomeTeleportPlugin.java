package com.gitlab.sm1le.hometeleport;

import com.earth2me.essentials.Essentials;
import com.gitlab.sm1le.hometeleport.command.CommandsHandler;
import com.gitlab.sm1le.hometeleport.message.MessagesStorage;
import com.gitlab.sm1le.message.MessageSender;
import com.gitlab.sm1le.message.MessagesFactory;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class HomeTeleportPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        new MessagesFactory(new File(getDataFolder(), "config.yml"), "messages").register(MessagesStorage.class);
        Essentials ess = (Essentials) getServer().getPluginManager().getPlugin("Essentials");
        MessageSender mSender = new MessageSender(getConfig(), "messages");
        new CommandsHandler(mSender, ess).register(this);
    }
}
