package com.gitlab.sm1le.hometeleport.command;

import com.earth2me.essentials.Essentials;
import com.earth2me.essentials.User;
import com.gitlab.sm1le.command.LiteCommand;
import com.gitlab.sm1le.command.util.PlayerCommandCondition;
import com.gitlab.sm1le.message.MessageSender;
import com.gitlab.sm1le.hometeleport.message.MessagesStorage;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class CommandsHandler {

    private LiteCommand<Player> homeTeleportCommand;
    private LiteCommand<Player> homeListCommand;

    public CommandsHandler(MessageSender mSender, Essentials ess) {
        this.homeTeleportCommand = LiteCommand.Builder.create("hometp", Player.class)
                .addCondition(PlayerCommandCondition.create(c -> c.getCommandSender().hasPermission("hometeleport.hometp"))
                    .elseExecute(c -> mSender.get(MessagesStorage.NO_PERMISSION).send(c.getCommandSender())))
                .execute(c -> {
                    Player sender = c.getCommandSender();
                    if(c.getArguments().length < 2) {
                        mSender.get(MessagesStorage.INVALID_SYNTAX, 2).send(sender);
                        return;
                    }
                    User user = ess.getUser(c.getArguments()[0]);
                    if(user == null) {
                        mSender.get(MessagesStorage.PLAYER_NOT_FOUND).send(sender);
                        return;
                    }
                    if(!user.getHomes().contains(c.getArguments()[1])) {
                        mSender.get(MessagesStorage.HOME_NOT_FOUND, c.getArguments()[1], user.getName()).send(sender);
                        return;
                    }
                    try {
                        Location loc = user.getHome(c.getArguments()[1]);
                        sender.teleport(loc);
                        mSender.get(MessagesStorage.YOU_TELEPORTED, c.getArguments()[1], user.getName()).send(sender);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).build();
        this.homeListCommand = LiteCommand.Builder.create("homelist", Player.class)
                .addCondition(PlayerCommandCondition.create(c -> c.getCommandSender().hasPermission("hometeleport.homelist"))
                    .elseExecute(c -> mSender.get(MessagesStorage.NO_PERMISSION).send(c.getCommandSender())))
                .execute(c -> {
                    Player sender = c.getCommandSender();
                    if(c.getArguments().length < 1) {
                        mSender.get(MessagesStorage.INVALID_SYNTAX, 1).send(sender);
                        return;
                    }
                    User user = ess.getUser(c.getArguments()[0]);
                    if(user == null) {
                        mSender.get(MessagesStorage.PLAYER_NOT_FOUND).send(sender);
                        return;
                    }
                    try {
                        mSender.get(MessagesStorage.HOME_LIST_HEADER, user.getName(), user.getHomes().size()).send(sender);
                        for(String home : user.getHomes()) {
                            Location loc = user.getHome(home);
                            mSender.get(MessagesStorage.HOME_LIST_LINE, home, loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()).send(sender);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }).build();
    }

    public void register(Plugin plugin) {
        this.homeTeleportCommand.register(plugin);
        this.homeListCommand.register(plugin);
    }

}
