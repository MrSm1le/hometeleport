package com.gitlab.sm1le.hometeleport.message;

import com.gitlab.sm1le.message.MessagesBase;

public enum MessagesStorage implements MessagesBase {

    NO_PERMISSION("&cНедостаточно прав"),
    INVALID_SYNTAX("&cКоличество аргументов должно быть не менее {0}"),
    PLAYER_NOT_FOUND("&cИгрок не найден"),
    HOME_NOT_FOUND("&cТочка дома {0} не найдена у игрока {1}"),
    YOU_TELEPORTED("&eВы телепортировались на точку дома {0} игрока {0}"),
    HOME_LIST_HEADER("&eТочки домов игрока {0}(Всего {1}):"),
    HOME_LIST_LINE("&e- {0}({1}, {2}, {3})");

    private String message;

    MessagesStorage(String message) {
        this.message = message;
    }

    @Override
    public String getPath() {
        return this.name().toLowerCase().replace("_", "-");
    }

    @Override
    public String getMessage() {
        return message;
    }
}
